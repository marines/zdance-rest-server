#!/bin/bash

ENV_LONG=$1

case $ENV_LONG in
	development)
		ENV_SHORT=dev;;
	production)
		ENV_SHORT=prod;;
	testing)
		ENV_SHORT=test;;
	*)
		echo -e "\033[0;33m▓ Oops! You should pass full name of environment (development|testing|production).\033[0m"
		exit 1;;
esac



PROJECT_ROOT=`git rev-parse --show-toplevel 2>&1`

if [[ "$PROJECT_ROOT" =~ "Not a git repository" ]]
then
	echo -e "\033[0;33m▓ Oops! You're outside of the project.\033[0m"
	exit 1
fi

echo -e "\033[0;33m▓ You sure? SQL will be loaded to \033[1m\033[4mzdance_$ENV_LONG\033[0m\033[0;33m. y/[n]\033[0m"
read -p "" choice < /dev/tty
case "$choice" in
	y|Y ) ;;
	* ) exit 1 ;;
esac



OLDPWD=`pwd`
cd $PROJECT_ROOT

echo -e "\n\033[0;32m▓ Loading SQL into database 'zdance_$ENV_LONG'\033[0m"
mysql -u root --password=root zdance_$ENV_LONG < seed/$ENV_LONG.sql
echo -e "\n\033[0;32m▓ Done\033[0m"

cd $OLDPWD
