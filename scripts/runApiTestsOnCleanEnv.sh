#!/bin/bash

PROJECT_ROOT=`git rev-parse --show-toplevel 2>&1`

if [[ "$PROJECT_ROOT" =~ "Not a git repository" ]]
then
	echo -e "\033[0;33m▓ Oops! You're outside of the project.\033[0m"
	exit 1
fi

OLDPWD=`pwd`
cd "$PROJECT_ROOT"

echo -e "\n\033[0;32m▓ Updating autoloader\033[0m"
composer dumpautoload -o

echo -e "\033[0;32m▓ Rolling back migrations\033[0m"
vendor/bin/phinx rollback -e testing

echo -e "\n\033[0;32m▓ Running migrations\033[0m"
vendor/bin/phinx migrate -e testing

echo -e "\n\033[0;32m▓ Seeding database\033[0m"
scripts/seedDatabase.sh testing

echo -e "\n\033[0;32m▓ Running tests\033[0m"
composer test

echo -e "\n\033[0;32m▓ Done\033[0m"

cd $OLDPWD
