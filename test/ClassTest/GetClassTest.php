<?php
namespace test\ClassTest;

class GetClassTest extends \test\BaseTest
{
    public function testSingleClassRetrieval()
    {
        $expectedResponse = [
            'id' => 1,
            'date' => '2013-12-14 10:30:00',
            'place_id' => 1,
            'rate' => 5000,
            'attendeesCount' => null,
            'paymentDate' => '2013-12-14 10:30:00',
            'created' => '2015-03-22 13:58:28',
            'updated' => null,
            'name' => 'Klub 1'
        ];
        $actualResponse = $this->client->get('class/1')->json();

        $this->assertEquals($expectedResponse, $actualResponse);
    }
}
