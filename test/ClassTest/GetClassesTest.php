<?php
namespace test\ClassTest;

class GetClassesTest extends \test\BaseTest
{
    public function testMultipleClassesRetrieval()
    {
        $expectedResponse = [
            [
                'id' => 1,
                'date' => '2014-01-18 10:30:00',
                'place_id' => 1,
                'rate' => 5000,
                'attendeesCount' => null,
                'paymentDate' => '2014-01-18 10:30:00',
                'created' => '2015-03-22 13:58:28',
                'updated' => null,
                'name' => 'Klub 1',
            ],
            [
                'id' => 1,
                'date' => '2014-01-18 10:30:00',
                'place_id' => 1,
                'rate' => 5000,
                'attendeesCount' => null,
                'paymentDate' => '2014-01-18 10:30:00',
                'created' => '2015-03-22 13:58:28',
                'updated' => null,
                'name' => 'Klub 1',
            ],
            [
                'id' => 1,
                'date' => '2013-12-14 10:30:00',
                'place_id' => 1,
                'rate' => 5000,
                'attendeesCount' => null,
                'paymentDate' => '2013-12-14 10:30:00',
                'created' => '2015-03-22 13:58:28',
                'updated' => null,
                'name' => 'Klub 1',
            ]
        ];
        $actualResponse = $this->client->get('class')->json();

        foreach ($expectedResponse as $class) {
            $this->assertEquals($class, array_shift($actualResponse));
        }
    }
}
