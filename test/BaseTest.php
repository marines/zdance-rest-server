<?php
namespace test;

require 'vendor/autoload.php';

abstract class BaseTest extends \PHPUnit_Framework_TestCase
{
    protected $client;
    protected $conf;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->conf = new \conf\Conf;
        $this->client = new \GuzzleHttp\Client(['base_url' => $this->conf->getBaseUrl()]);
    }
}
