<?php
namespace test;

class DummyTest extends BaseTest
{

    /**
     * @expectedException   Exception
     */
    public function testTrueIsFalse()
    {
        throw new \Exception;
        $this->assertEquals(true, false);
    }

    public function testTrueIsTrue()
    {
        $this->assertEquals(true, true);
    }
}
