<?php
use Phinx\Migration\AbstractMigration;

class Classes extends AbstractMigration
{

    /**
     * Migrate Up.
     */
    public function up()
    {

        $places = $this->table('places');
        $places->addColumn('name', 'string', ['limit' => 32])
                ->save();

        $classes = $this->table('classes');
        $classes->addColumn('date', 'datetime')
                ->addColumn('place_id', 'integer')
                ->addColumn('rate', 'integer')
                ->addColumn('attendeesCount', 'integer', ['null' => true])
                ->addColumn('paymentDate', 'datetime', ['null' => true])
                ->addColumn('created', 'datetime')
                ->addColumn('updated', 'datetime', ['null' => true])
                ->addIndex(['place_id'])
                ->save();

        // phinx' foreign key mechanism doesn't work
        $this->execute('ALTER TABLE `classes` ADD CONSTRAINT `places_id` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`)');
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute('ALTER TABLE `classes` DROP FOREIGN KEY `places_id`');
        $this->dropTable('places');
        $this->dropTable('classes');
    }
}
