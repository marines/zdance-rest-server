<?php
return [
    'baseUrl' => 'http://zdance.test/api/',
    'mode' => 'testing',
    'debug' => false,
    'db' => [
        'connectionString' => 'mysql:host=localhost;dbname=zdance_testing',
        'user' => 'root',
        'pass' => 'root'
    ]
];
