<?php
return [
    'baseUrl' => 'http://zdance.prod/api/',
    'mode' => 'production',
    'debug' => false,
    'db' => [
        'connectionString' => 'mysql:host=localhost;dbname=zdance_production',
        'user' => 'root',
        'pass' => 'root'
    ]
];
