<?php
return [
    'baseUrl' => 'http://zdance.dev/api/',
    'mode' => 'development',
    'debug' => true,
    'db' => [
        'connectionString' => 'mysql:host=localhost;dbname=zdance_development',
        'user' => 'root',
        'pass' => 'root'
    ]
];
