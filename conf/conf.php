<?php
namespace conf;

class Conf
{
    private $environment;
    private $configuration;

    public function __construct()
    {
        $this->environment = getenv('APP_ENV');
        if (false === $this->environment) {
            $this->environment = 'production';
        }

        $configurationFileName = 'conf.'.$this->environment.'.php';
        $this->configuration = require $configurationFileName;
    }

    public function getMode()
    {
        return $this->configuration['mode'];
    }

    public function getDebug()
    {
        return $this->configuration['debug'];
    }

    public function getDbConf()
    {
        return $this->configuration['db'];
    }

    public function getBaseUrl()
    {
        return $this->configuration['baseUrl'];
    }
}
