-- PLACES
INSERT INTO `zdance_testing`.`places`
    (`id`, `name`)
VALUES
    (1, 'Klub 1'),
    (2, 'Klub 2');

-- CLASSES
INSERT INTO `zdance_testing`.`classes`
    (`id`, `date`, `place_id`, `rate`, `attendeesCount`, `paymentDate`, `created`, `updated`)
VALUES
    (1, '2013-12-14 10:30:00', '1', '5000', NULL, '2013-12-14 10:30:00', '2015-03-22 13:58:28', NULL),
    (2, '2014-01-18 10:30:00', '1', '5000', NULL, '2014-01-18 10:30:00', '2015-03-22 13:58:28', NULL),
    (3, '2014-01-18 10:30:00', '1', '5000', NULL, '2014-01-18 10:30:00', '2015-03-22 13:58:28', NULL);
