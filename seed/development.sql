-- PLACES
INSERT INTO `zdance_development`.`places`
    (`id`, `name`)
VALUES
    (NULL, 'Klub SuperFit'),
    (NULL, 'Szkoła tańca Danuta'),
    (NULL, 'Fitness Trójmiasto');

-- CLASSES
INSERT INTO `zdance_development`.`classes`
    (`id`, `date`, `place_id`, `rate`, `attendeesCount`, `paymentDate`, `created`, `updated`)
VALUES
    (NULL, '2013-12-14 10:30:00', '1', '5000', NULL, '2013-12-14 10:30:00', NOW(), NULL),
    (NULL, '2014-01-18 10:30:00', '1', '5000', NULL, '2014-01-18 10:30:00', NOW(), NULL),
    (NULL, '2014-01-18 10:30:00', '1', '5000', NULL, '2014-01-18 10:30:00', NOW(), NULL);
