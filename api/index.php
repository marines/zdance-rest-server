<?php
namespace api;

require '../vendor/autoload.php';

$conf = new \conf\Conf;

$api = new \Slim\Slim([
    'debug' => $conf->getDebug(),
    'mode' => $conf->getMode()
]);

$api->container->singleton('Conf', function ($container) use ($conf) {
    return $conf;
});

$api->container->singleton('ClassesDao', function ($container) {
    return new \app\dao\Classes($container['Conf']->getDbConf());
});

$api->response->headers->set('Content-Type', 'application/json');

$api->get('/', function () {
    header("Content-Type: application/json");
    echo json_encode(['hello world']);
});

$api->group('/class', function () use ($api) {
    $classes = $api->container['ClassesDao'];

    $api->get('/', function () use ($api, $classes) {
        echo json_encode($classes->getClasses());
    });

    $api->post('/', function () use ($api) {

    });

    $api->get('/:id', function ($id) use ($api, $classes) {
        echo json_encode($classes->getClassById($id));
    });

    $api->put('/:id', function ($id) use ($api) {

    });

    $api->delete('/:id', function ($id) use ($api) {

    });
});

$api->run();
