<?php
namespace app\dao;

class BaseDao
{
    public function __construct($dbConfig)
    {
        \ORM::configure([
            'connection_string' => $dbConfig['connectionString'],
            'username' => $dbConfig['user'],
            'password' => $dbConfig['pass']
        ]);
    }
}
