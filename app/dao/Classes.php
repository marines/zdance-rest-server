<?php
namespace app\dao;

class Classes extends BaseDao
{
    public function getClassById($id)
    {
        $class = \ORM::forTable('classes')
        ->join('places', ['classes.place_id', '=', 'places.id'])
        ->where('classes.id', $id)
        ->findArray($id);

        return reset($class);
    }

    public function getClasses()
    {
        $class = \ORM::forTable('classes')
        ->join('places', ['classes.place_id', '=', 'places.id'])
        ->orderByDesc('classes.date')
        ->findArray();

        return $class;
    }
}
